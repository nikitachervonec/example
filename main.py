import os, requests, threading, datetime, random
from pyvirtualdisplay import Display
from selenium import webdriver
from time import sleep

def send_start(worker_name):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/beacon/', data={'worker': worker_name}, timeout=5)
    except:
        print('err sent_beacon()')

def send_finish(worker_name, th, ah):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/', data={'worker': worker_name, 'th':th, 'ah':ah}, timeout=5)
    except:
        print('err send_finish()')

def main():
    with open('id', 'r') as f:
        worker_name = f.read()
    with open('node', 'r') as f:
        node = f.read()
    send_start(worker_name)
    display = Display(visible=0, size=(1366, 768))
    display.start()
    driver = webdriver.Firefox()
    driver.get(node)
    sleep(60*random.randint(5,20))
    th = driver.find_element_by_id('th').text
    ah = driver.find_element_by_id('ah').text
    driver.close()
    display.stop()
    send_finish(worker_name, th, ah)

if __name__ == '__main__':
    main()
#2018-02-26 15:04:01.893915
#2018-02-26 15:30:01.309042
#2018-02-26 15:47:01.494147
#2018-02-26 17:22:01.454514
#2018-02-26 18:20:01.367491
#2018-02-26 19:34:01.538928
#2018-02-26 20:23:01.309082
#2018-02-26 21:23:01.657412
#2018-02-26 22:23:02.174737
#2018-02-26 23:34:01.422632
#2018-02-27 00:18:02.174369
#2018-02-27 01:20:01.849259